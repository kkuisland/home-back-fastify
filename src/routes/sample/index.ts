import { FastifyPluginAsync } from "fastify";
import * as sampleController from "../../controllers/sample";

const example: FastifyPluginAsync = async (fastify, opts): Promise<void> => {
  // 생성
  fastify.post("/", sampleController.store);
  // 전체 리스트
  fastify.get("/", sampleController.fetch);
  // 인덱스 검색
  fastify.get("/:_id", sampleController.get);
  // 수정
  fastify.put("/", sampleController.update);
  // 삭제
  fastify.delete("/:_id", sampleController.destroy);
};

export default example;

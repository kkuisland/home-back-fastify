import { FastifyPluginAsync } from "fastify";

const example: FastifyPluginAsync = async (fastify, opts): Promise<void> => {
  fastify.get("/", async function (request, reply) {
    const token = fastify.jwt.sign({ id: "test" });
    console.log("token ->", token);
    return "this is an example";
  });
};

export default example;

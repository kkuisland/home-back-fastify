import * as _ from "lodash";

import Sample from "../models/sample";

// Sample 생성
export const store = async (request: any, reply: any) => {
  const reqBody = request.body;
  let sample = null;
  try {
    sample = await Sample.create(reqBody);
  } catch (error) {
    console.log("PartsController-Create-Error ->", error);
  }
  if (sample) {
    reply.send({ bool: true, value: sample, message: "Sample 생성 완료" });
  } else {
    reply.send({ bool: false, value: null, message: "Sample 생성 실패" });
  }
};
// Sample 리스트
export const fetch = async (request: any, reply: any) => {
  let sample = null;
  try {
    sample = await Sample.find();
  } catch (error) {
    console.log("SampleController-Fetch-Error ->", error);
  }
  if (sample) {
    const newSample = sample.map((item) => {
      let rObj = {};
      console.log("item ->", item);
      rObj = { item };
      return rObj;
    });
    reply.send({
      bool: true,
      value: newSample,
      message: "Sample 리스트 검색 완료",
    });
  } else {
    reply.send({
      bool: false,
      value: null,
      message: "Sample 리스트 검색 실패",
    });
  }
};
// Sample 인덱스 검색
export const get = async (request: any, reply: any) => {
  const { _id } = request.params;

  let sample = null;
  try {
    sample = await Sample.findById(_id);
  } catch (error) {
    console.log("SampleController-Get-Error ->", error);
  }
  if (sample) {
    // 토큰 데이터
    let tokenData = {
      id: sample.name,
      phone: "010-9947-0728",
    };
    // 토큰 데이터 추가하여 표기
    const newSample = _.merge({}, sample, { token: tokenData });
    // 토큰 생성
    let token = await reply.jwtSign(tokenData);
    // 토큰을 이용한 쿠키 생성
    await reply
      .setCookie("kkuToken", token, {
        // domain: '*',
        path: "/",
        httpOnly: true,
        secure: true,
        sameSite: "None",
        overwrite: true,
        maxAge: 24 * 60 * 60 * 5,
        signed: true,
      })
      .send({ bool: true, value: newSample, message: "Sample2 검색 완료" });
  } else {
    reply.send({ bool: false, value: null, message: "Sample 검색 실패" });
  }
};
// Sample 수정
export const update = async (request: any, reply: any) => {
  const reqBody = request.body;
  let sample = null;
  try {
    sample = await Sample.findByIdAndUpdate(reqBody._id, { ...reqBody });
  } catch (error) {
    console.log("PartsController-Update-Error ->", error);
  }
  if (sample) {
    reply.send({ bool: true, value: sample, message: "Sample 수정 완료" });
  } else {
    reply.send({ bool: false, value: null, message: "Sample 수정 실패" });
  }
};
// Sample 인덱스 검색
export const destroy = async (request: any, reply: any) => {
  const { _id } = request.params;
  let sample = null;
  try {
    sample = await Sample.deleteOne({ _id: _id });
  } catch (error) {
    console.log("SampleController-Destroy-Error ->", error);
  }
  if (sample) {
    reply.send({ bool: true, value: sample, message: "Sample 제거 완료" });
  } else {
    reply.send({ bool: false, value: null, message: "Sample 제거 실패" });
  }
};

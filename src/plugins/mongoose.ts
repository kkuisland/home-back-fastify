import fp from "fastify-plugin";
import { connect, ConnectOptions } from "mongoose";

/**
 * 몽고 DB
 *
 * @see https://mongoosejs.com/docs
 */
module.exports = fp<ConnectOptions>(async function (fastify, opts) {
  connect(`${process.env.MONGODB_URL}`, {
    // useFindAndModify: false,
    // useNewUrlParser: true,
    // useUnifiedTopology: true,
  })
    .then((response) => {
      console.log("몽고 DB 연결 성공");
    })
    .catch((error) => {
      console.error("Error plugins mongoose", error);
    });
});

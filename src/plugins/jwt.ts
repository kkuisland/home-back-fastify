import fp from "fastify-plugin";
import fastifyJwt, { FastifyJWTOptions } from "fastify-jwt";
/**
 * This plugins adds some utilities to handle http errors
 *
 * @see https://github.com/fastify/fastify-sensible
 */
export default fp<FastifyJWTOptions>(async (fastify, opts) => {
  const secretKey = Buffer.from(`${process.env.JWT_SECRET_KEY}`).toString(
    "base64"
  );
  fastify.register(fastifyJwt, {
    secret: secretKey,
    decode: { complete: true },
    sign: {
      // algorithm: "HS256",
      // issuer: "kkuisland.com",
      expiresIn: "7d",
      // subject: "kkuToken",
    },
    verify: {
      //   algorithms: "HS256",
      //   issuer: "kkuisland.com",
      maxAge: "7d",
      //   subject: "kkuToken",
    },
    cookie: {
      cookieName: "kkuToken",
      signed: true,
    },
  });
});

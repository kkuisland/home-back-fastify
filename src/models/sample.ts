import { Schema, model, Document } from "mongoose";
import * as moment from "moment";

interface Sample extends Document {
  name: string;
  sequence: number;
  title: string;
  remarks?: string;
  createdAt: string;
  updatedAt: string;
}

const schema = new Schema<Sample>({
  // 이름
  name: { type: String, required: true, index: true },
  // 순번
  sequence: { type: Number, required: true },
  // 제목
  title: { type: String, required: true },
  // 비고사항
  remarks: { type: String, default: null },
  // 생성일자
  createdAt: { type: String, default: moment().format("YYYY-MM-DD hh:mm:ss") },
  // 수정일자
  updatedAt: { type: String, default: moment().format("YYYY-MM-DD hh:mm:ss") },
});

export default model<Sample>("Sample", schema, "Sample");
